//
//  AppDelegate.h
//  Challenges
//
//  Created by appkoder on 18/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

