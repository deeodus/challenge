//
//  ViewController.m
//  Challenges
//
//  Created by appkoder on 18/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"%li", [self numberCompliment:50]);
    NSLog(@"%li", [self numberCompliment:100]);
    NSLog(@"%li", [self numberCompliment:5]);
    NSLog(@"%li", [self numberCompliment:25]);
}

-(long)numberCompliment:(NSUInteger)input
{
    
    NSString *string = @"";

    //use bit shifting to convert an decimal to binary
    int i = 0;
    while (input > 0) {
        
        string = [[NSString stringWithFormat:@"%lu", input&1] stringByAppendingString:string];
        
        input = input>> 1;
        
        ++i;
    }
    
    
    NSMutableArray *array = [NSMutableArray array];
    
    //break the binary string into array of individual characters
    for (int i = 0; i < [string length]; i++) {
        NSString *characters = [string substringWithRange:NSMakeRange(i, 1)];
        [array addObject:characters];
    }
    
    
    NSMutableArray *flippedArray = [NSMutableArray array];
    //go through the arrays and flip each number, 0 becomes 1 and 1 becomes 0
    for (int i = 0; i < array.count; i++) {
        
        int value = [[array objectAtIndex:i] intValue];
        if (value == 0) {
            
            [flippedArray addObject:[NSString stringWithFormat:@"%i", 1]];
        }else{
            
            [flippedArray addObject:[NSString stringWithFormat:@"%i", 0]];
        }
        
    }
    
    //combine the flipped value in the array into one string
    NSString *flippedString = [flippedArray componentsJoinedByString:@""];
    
    
    //use the C function to convert a string to base 2
    long result = strtol([flippedString UTF8String], NULL, 2);
    
    return result;
}


@end
