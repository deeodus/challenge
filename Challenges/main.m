//
//  main.m
//  Challenges
//
//  Created by appkoder on 18/05/2017.
//  Copyright © 2017 appkoder. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
